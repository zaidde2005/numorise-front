import Vue from 'vue'
import App from './App'

import VueResource from 'vue-resource'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.css'
import auth from './auth'
import Vuetify from 'vuetify'
import store from './store'
import router from './router.vue'
import Conf from './plugins/conf'

Vue.use(Conf)
Vue.use(Vuetify)
Vue.use(VueResource)
Vue.use(VueMaterial)
Vue.http.options.root = Conf.conf.host

auth.checkAuth()
Vue.material.registerTheme('default', {
  primary: {
    color: 'yellow',
    hue: '800'
  },
  accent: 'red',
  warn: 'red',
  background: 'white'
})

new Vue({
  router,
  store,
  components: {App}
}).$mount('#app')
