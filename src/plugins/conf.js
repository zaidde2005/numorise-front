import confFile from '../conf'
export default {
  conf: confFile,
  install (Vue, options) {
    Vue.prototype.$conf = this.conf
  }
}
