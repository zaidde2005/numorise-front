 /* global localStorage */
  export default {
    authenticated: false,
    token: null,

    login: function (instance, data) {
      return instance.$http.post('login', data).then(response => {
        localStorage.setItem('token', response.data.token)
        this.checkAuth()
      })
    },
    checkAuth: function () {
      if (!this.token) {
        this.getToken()
      }
      if (this.token && (this.token.exp * 1000) > +new Date()) {
        this.authenticated = true
      }
      return this.authenticated
    },
    parseJwt: function (token) {
      var base64Url = token.split('.')[1]
      var base64 = base64Url.replace('-', '+').replace('_', '/')
      return JSON.parse(window.atob(base64))
    },
    getToken: function () {
      var token = localStorage.getItem('token')
      if (token) {
        this.token = this.parseJwt(token)
      }
    },
    logOut: function () {
      localStorage.removeItem('token')
      this.authenticated = false
      this.token = false
    },
    register: function (instance, data) {
      return instance.$http.post('register', data).then(response => {
        localStorage.setItem('token', response.data.token)
        this.checkAuth()
      })
    }
  }
